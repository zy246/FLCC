import numpy as np
from collections import Counter

import keras

# data=[1,2,3,4,5]
# data2=[6,7,8,9,10]
# array = np.zeros((2,5))
# array[0,:] = data
# temp=[]
# rec_list=[]
# for i in array.tolist():
#     for j in i:
#         temp.append(j)
# Ranking = Counter(temp)
# Ranking_top = Ranking.most_common(2)
# for i in Ranking_top:
#     rec_list.append(i[0])
#
# print(rec_list)

# a = (-0.00344265-0.00121992-0.00179763-0.00116375)/4
# print(a)

sum = 0

list=[1,5]
batch_size = 200

RatingMatrix = np.loadtxt('data/FLC_Data/train_data_v2.txt')

RatingMatrix_test1 = RatingMatrix[0:200]
print(RatingMatrix_test1.shape)
RatingMatrix_test1[RatingMatrix_test1 > 0] = 1
num_test1= np.sum(RatingMatrix_test1)

RatingMatrix_test2 = RatingMatrix[800:1000]
print(RatingMatrix_test2.shape)
RatingMatrix_test2[RatingMatrix_test2 > 0] = 1
num_test2= np.sum(RatingMatrix_test2)

sum_test = num_test2 + num_test1

print(sum_test)

for i in list:
    RatingMatrix_new = RatingMatrix[batch_size*(i-1):batch_size*i]
    print(RatingMatrix_new.shape)
    RatingMatrix_new[RatingMatrix_new > 0] = 1
    num= np.sum(RatingMatrix_new)
    sum = sum+num

print(sum)


