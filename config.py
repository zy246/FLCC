from keras.datasets import mnist
from keras.models import Model #泛型模型
from keras.layers import Dense, Input
from CDAE import aCSDAE
from keras import backend as K
from CDAE import mask, loss
from keras import optimizers


#def loss(y_true, y_pred):
#    y_mask = y_true / (y_true + 1e-9)
#    return K.sum(K.square(y_true - (y_mask * y_pred)), axis=-1)


def get_autoencoder(input_dim, hidden_layer: list,name):
    model = aCSDAE.create(I=input_dim, K=hidden_layer, hidden_activation="relu",
                  output_activation="sigmoid", q=drop_out_rate, l=regularization_term, use_user = user_method, U = user_info)
    #adam = optimizers.Adam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    #model.compile(loss="mse", optimizer=adam)
    model.compile(loss="mse", optimizer='adam')

    return model

def get_mask(data):
    return mask.get_mask(data, mask_method)

item_network_input_dim = 3883
item_network_hidden_layer = [32]
drop_out_rate = 0.2
regularization_term = 1e-4
user_method = True
mask_method = "user"
user_info =29
epochs = 1
batch_size = 200

client_num = 5
sbs_num = 2
top_num = 50
mbs_num = 150
round_num = 100