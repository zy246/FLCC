from keras.layers import Input, Dense, Embedding, Flatten, Dropout, merge, Activation
from keras.models import Model
from keras.regularizers import l2

def create(I, K, hidden_activation, output_activation, q=0.2, l=0.01, use_user=True, U=29):
    x_item = Input((I,), name='x_item')
    if use_user :
        x_user = Input((U,), name='x_user')
    x_mask = Input((I,), name='x_mask')
    h_item = Dropout(q)(x_item)
    h = h_item
    for k in K:
        h_item = Dense(k, W_regularizer=l2(l), b_regularizer=l2(l))(h)
        if use_user:
            h_user = Dense(k, W_regularizer=l2(l), b_regularizer=l2(l))(x_mask)
            h = merge([h_item, h_user], mode='sum')
        else:
            h = h_item
        if hidden_activation:
            h = Activation(hidden_activation)(h)
    y = Dense(I, activation=output_activation)(h)
    y_masked = merge([y, x_mask], mode="mul")
    if use_user:
        model = Model(input=[x_item, x_user, x_mask], output=y_masked)
    else:
        model = Model(input=[x_item, x_mask], output=y_masked)
    return model