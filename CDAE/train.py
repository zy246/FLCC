import numpy
numpy.random.seed(0)
from keras import optimizers
from CDAE import metrics, generate_data, mask
import CDAE.mask

# data
train_users, train_x, test_users, test_x = generate_data.load_data_1m_2()
train_x_users = numpy.array(train_users, dtype=numpy.int32).reshape(len(train_users), 1)
test_x_users = numpy.array(test_users, dtype=numpy.int32).reshape(len(test_users), 1)
train_mask = mask.get_mask(train_x, method="user")
pred_mask = numpy.ones(train_x.shape)

# model
#model = aCSDAE.create(I=train_x.shape[1], U=len(train_users)+1, u=256, K=[128], hidden_activation='relu', output_activation='sigmoid', q=0.1, l=0.0001)
model = CDAE.create(I=train_x.shape[1], U=len(train_users)+1, K=128, hidden_activation='relu', output_activation='sigmoid', q=0.1, l=0.0001)
adam = optimizers.adam(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='mse', optimizer=adam)


# train
history = model.fit(x=[train_x, train_x_users, train_mask], y=train_x,
                    batch_size=512, nb_epoch=500, verbose=1,
                    validation_data=[[test_x, test_x_users, train_mask], test_x])

# predict
pred = model.predict(x=[train_x, numpy.array(train_users, dtype=numpy.int32).reshape(len(train_users), 1), pred_mask])
pred = pred * (train_x == 0) # remove watched items from predictions
print(pred)
print(pred.max(axis=1))
pred = numpy.argsort(pred)

n = 10
sr = metrics.success_rate(pred[:, -n:], test_x)
print("Success Rate at {:d}: {:f}".format(n, sr))
cf = metrics.cache_efficiency(pred[:, -n:], test_x)
print("Cache Efficiency at {:d}: {:f}".format(n, cf))

all = numpy.arange(pred.shape[1])
for i in range(pred.shape[0]):
    pred[i] = numpy.random.choice(all, pred.shape[1], replace=False)

sr = metrics.success_rate(pred[:, -n:], test_x)
print("Success Rate at {:d}: {:f}".format(n, sr))
cf = metrics.cache_efficiency(pred[:, -n:], test_x)
print("Cache Efficiency at {:d}: {:f}".format(n, cf))