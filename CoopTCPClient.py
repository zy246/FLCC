import socket
import pickle
import io
import time
import struct
import protocol
import select
import config
import argparse
import numpy as np
import os
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

client_num = 1

top_num = config.top_num
mbs_num = config.mbs_num
round_num = config.round_num

os.environ["CUDA_VISIBLE_DEVICES"] = "0"
config_gpu = tf.ConfigProto()
config_gpu.gpu_options.per_process_gpu_memory_fraction = 0.2
set_session(tf.Session(config=config_gpu))

class Data:
    def __init__(self,index,client_index_list,datasize):

        np.random.seed(1337)  # for reproducibility
        self.TopN = top_num
        self.mbsN = mbs_num
        self.select_num = self.TopN
        self.rec_list = []
        self.total_rec_list = []
        self.loadData(index,client_index_list,datasize)
        self.initNetwork()
        self.recall_list_sbs = []
        self.recall_list_neighbor_sbs=[]
        self.recall_list_mbs = []


    def loadData(self, client_index, client_index_list, datasize):
        # self.RatingMatrix = np.loadtxt('data/ml_dataset/train_user' + str(client_index) + '.txt')
        # self.TestMatrix = np.loadtxt('data/ml_dataset/test_user' + str(client_index) + '.txt')
        # self.UserAttr = np.loadtxt('data/ml_dataset/attr_user' + str(client_index) + '.txt')

        RatingMatrix = np.loadtxt('data/FLC_Data/train_data_v2.txt')
        TestMatrix = np.loadtxt('data/FLC_Data/test_data_v2.txt')
        UserAttr = np.loadtxt('data/FLC_Data/user_attr_v2.txt')

        print('data start:',datasize*(client_index-1))
        print('data end:', datasize*client_index)

        self.RatingMatrix= RatingMatrix[datasize*(client_index-1):datasize*client_index,:]
        self.TestMatrix= TestMatrix[datasize*(client_index-1):datasize*client_index,:]
        self.UserAttr = UserAttr[datasize*(client_index-1):datasize*client_index,:]

        print('RatingMatrix Shape:', self.RatingMatrix.shape)
        print('TestMatrix Shape:', self.TestMatrix.shape)
        print('UserAttr Shape:', self.UserAttr.shape)

        self.RatingMatrix[self.RatingMatrix > 0] = 1
        self.total_train_num = np.sum(self.RatingMatrix)

        sum=0
        for i in client_index_list:
            RatingMatrix_new = RatingMatrix[datasize * (i - 1):datasize * i]
            print(RatingMatrix_new.shape)
            RatingMatrix_new[RatingMatrix_new > 0] = 1
            num = np.sum(RatingMatrix_new)
            sum = sum + num

        print("~~~~~~~~~~~~~~~~~~~SUM~~~~~~~~~~~~~~",sum)
        self.weight_tr_num = self.total_train_num/sum
        print("~~~~~~~~~~~~~~~~~~~Weight SUM ~~~~~~~~~~~~~~", self.weight_tr_num)
        self.TestMatrix[self.TestMatrix > 0] = 1
        self.RatingMatrix = self.RatingMatrix.astype(np.int64)
        self.TestMatrix = self.TestMatrix.astype(np.int64)
        self.RatingMask = config.get_mask(self.RatingMatrix)
        self.TestMask = np.ones(self.TestMatrix.shape)
        requestList = []
        for i in range(self.TestMatrix.shape[0]):
            wuid = np.where(self.TestMatrix[i, :] > 0)
            wuid = np.array(wuid)
            for i in wuid[0]:
                requestList.append(i)
        self.TestList = requestList
        self.RatingMatrixItem = self.RatingMatrix.copy()

    def initNetwork(self):
        # deep autoencoders based on Keras
        self.autoencoder = config.get_autoencoder(config.item_network_input_dim, config.item_network_hidden_layer, "item")


    def setNetwork(self, weight):
        self.autoencoder.set_weights(weight)

    def getNetwork(self):

        weight = self.autoencoder.get_weights()
        return weight

    def train(self):

        x = self.RatingMatrixItem
        print('Training Matrix: ', x.shape)

        # training
        self.autoencoder.fit(x=[x,self.UserAttr,self.RatingMask], y=x, epochs=config.epochs, batch_size= config.batch_size, shuffle=True)

    def predict(self):
        t1 = time.time()
        pred = self.autoencoder.predict(x=[self.RatingMatrixItem,self.UserAttr, self.TestMask])
        pred = pred * (self.RatingMatrixItem == 0)  # remove watched items from predictions
        pred = np.argsort(pred)
        rec = pred[:, -self.mbsN:]
        recommended = np.zeros(self.RatingMatrixItem.shape)
        for i in range(rec.shape[0]):
            recommended[i][rec[i]] = 1
        rec_sum = recommended.sum(axis=0)
        rec_sum_sort = np.argsort(rec_sum)
        cache = rec_sum_sort[-self.select_num:]
        self.rec_list = cache.tolist()
        t2 = time.time()
        t = t2-t1
        print('predict time:', t)

    def test_data(self,serverList):

        print('TO BE TEST LIST:', len(serverList))
        self.sbs_list = serverList[0:self.TopN]
        print('Length of sbs list:',len(self.sbs_list))
        self.sbs_neighbor_list = serverList[0:client_num*self.TopN+self.TopN]
        print('Length of sbs neighbor list:', len(self.sbs_neighbor_list))
        self.mbs_list=serverList
        print('Length of mbslist:', len(self.mbs_list))

    def test(self,list,flag_bs):

        news_lists = []
        for id in list:
            if id not in news_lists:
                news_lists.append(id)

        count = self.TestMatrix.shape[0]
        rcl = 0

        for i in range(0,self.TestMatrix.shape[0]):  # self.rec_top_N(2)
            rating = self.TestMatrix[i, :]
            vindex = np.where(rating > 0)
            vindex = np.array(vindex)
            if vindex.shape[1] == 0:
                print("Ignored all zero ...")
                count -= 1

            rtrue = 0

            for rlist in news_lists:
                for ulist in vindex[0, :]:
                    if rlist == ulist:
                        rtrue += 1
            if vindex.shape[1] > 0:
                recall = rtrue / vindex.shape[1]
                rcl = rcl + recall

        print("The cache efficiency@", self.TopN, " is: ", rcl / count)
        if (rcl/ count > 1):
            np.savetxt("data/wrong.txt", list)

        if flag_bs == protocol.TEST_SBS:
            self.recall_list_sbs.append(rcl / count)
            print("The recall SBS list:", self.recall_list_sbs)
        elif flag_bs == protocol.TEST_NEIGHBOR_SBS:
            self.recall_list_neighbor_sbs.append(rcl / count)
            print("The recall neighbor SBS list:",self.recall_list_neighbor_sbs)
        elif flag_bs == protocol.TEST_MBS:
            self.recall_list_mbs.append(rcl/count)
            print("The recall MBS list:", self.recall_list_mbs)


class TCPClient:
    def __init__(self,ipaddress, port,index,client_index_list,datasize):
        self.round_num = round_num
        address = (ipaddress, port)
        self.outputs = []
        self.inputs = []
        self.socks = []
        self.outputs = []
        self.c_index = index
        self.send_dict = {}
        self.round_dict ={}
        self.data_class = Data(index,client_index_list,datasize)

        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(address)
        self.inputs.append(client_socket)
        self.send_dict[client_socket] = {"flag": protocol.SEND_INIT, "mess_data": []}
        self.round_dict[client_socket] = 0
        #count+=1

    def _recvall(self, sock, count):
        buf = b''
        while count:
            newbuf = sock.recv(count)
            if not newbuf: return None
            buf += newbuf
            count -= len(newbuf)
        return buf

    def tcp_read(self, sock):
        lengthbuf = self._recvall(sock, 4)
        length, = struct.unpack('!I', lengthbuf)
        data = self._recvall(sock, length)
        message = pickle.loads(data)
        self.outputs.append(sock)

        return message

    def tcp_send(self, sock, message):
        data = pickle.dumps(message)
        length = len(data)
        sock.sendall(struct.pack('!I', length))
        sock.sendall(data)
        if sock in self.outputs:
            self.outputs.remove(sock)

    def para_handler(self,socket,data):
        self.send_dict[socket]["flag"] = protocol.SEND_PARA
        self.data_class.setNetwork(data)
        self.data_class.train()
        self.data_class.predict()

        WB = self.data_class.getNetwork()
        weight_WB = self.weight_para_handler(WB)
        self.send_dict[socket]["mess_data"] = weight_WB

    def weight_para_handler(self, WB):
        weight_WB = []
        for wb_i in WB:
            wb = wb_i * self.data_class.weight_tr_num
            weight_WB.append(wb)
        return weight_WB

        #self.send_dict[socket]["mess_data"] = WB

    def rec_list_to_sbs_handler(self,socket,data):
        self.send_dict[socket]["flag"] = protocol.CLIENT_LIST_TO_SBS
        self.data_class.setNetwork(data)
        self.data_class.train()
        self.data_class.predict()
        print('Predict cache list:', len(self.data_class.rec_list))
        self.send_dict[socket]["mess_data"] = self.data_class.rec_list

    def test_handler(self,socket,data):
        self. data_class.test_data(data)
        print('#################### TEST SBS ##########################')
        self.data_class.test(self.data_class.sbs_list,protocol.TEST_SBS)
        print('#################### TEST SBS Neighbor ##########################')
        self.data_class.test(self.data_class.sbs_neighbor_list, protocol.TEST_NEIGHBOR_SBS)
        print('#################### TEST MBS ##########################')
        self.data_class.test(self.data_class.mbs_list,protocol.TEST_MBS)

        if (len(self.data_class.recall_list_mbs)==self.round_num and len(self.data_class.recall_list_neighbor_sbs)==self.round_num and len(self.data_class.recall_list_sbs)==self.round_num):
            self.send_dict[socket]["flag"] = protocol.Round_Finish
            self.send_dict[socket]["mess_data"] = ['ROUND END']
            np.savetxt('result/'+str(top_num)+'_SBS_User' + str(self.c_index) + '.txt', self.data_class.recall_list_sbs)
            np.savetxt('result/' + str(top_num) + '_NeSBS_User' + str(self.c_index) + '.txt', self.data_class.recall_list_neighbor_sbs)
            np.savetxt('result/' + str(top_num) + '_MBS_User' + str(self.c_index) + '.txt', self.data_class.recall_list_mbs)
            print("####### END!!!! ######")
            if socket in self.outputs:
                self.outputs.remove(socket)
            self.inputs.remove(socket)
        else:
            self.send_dict[socket]["flag"] = protocol.SEND_PARA
            WB = self.data_class.getNetwork()
            weight_WB = self.weight_para_handler(WB)
            self.send_dict[socket]["mess_data"] = weight_WB


    def run(self, timeout=30):
        while self.inputs:
            #print("Waiting events!")
            readable, writable, exceptional = select.select(self.inputs, self.outputs, self.inputs, timeout)

            if not (readable or writable or exceptional):
                print("Time out ! ")
                break

            for s in readable:
                data = self.tcp_read(s)

                print('Recevie FLAG:',data["flag"])
                if data["flag"] == protocol.SEND_INIT:
                    print(s.getpeername(), "received init data")
                    self.para_handler(s,data["mess_data"])

                elif data["flag"] == protocol.SEND_PARA:
                    print(s.getpeername(), "received para data", data)
                    self.para_handler(s,data["mess_data"])

                elif data["flag"] == protocol.SEND_REC:
                    print(s.getpeername(), "received final para data", data)
                    self.rec_list_to_sbs_handler(s,data["mess_data"])

                elif data["flag"] == protocol.SBS_TO_CLIENT_SEND_FINAL:
                    print(s.getpeername(), "received", data)
                    self.test_handler(s, data["mess_data"])
                else:
                    # Interpret empty result as closed connection
                    print("-----closing-----")
                    if s in self.outputs:
                        self.outputs.remove(s)
                    self.inputs.remove(s)
                    # s.close()

            for s in writable:
                print('send data:', self.send_dict[s])
                self.tcp_send(s, self.send_dict[s])